
 -- rato
 local petRato1 = {"Rato1", "Rato2", "Rato3"} 

 local petRato2 = {"Cave Rat (1)", "Cave Rat (2)", "Cave Rat (3)"} 

 local ratoFala = {"Mep!", "Meep!", "Meeep!"} 

 -- lobo
 local petLobo1 = {"Lobo1", "Lobo2", "Lobo3"} 

 local petLobo2 = {"Wolf (1)", "Wolf (2)", "Wolf (3)"} 

 local loboFala = {"Gr!", "Grr!", "Grrr!"} 

 -- fire d
 local petFd1 = {"fd1", "fd2", "fd3"} 

 local petFd2 = {"Fire Devil (1)", "Fire Devil (2)", "Fire Devil (3)"} 

 local fdFala = {"Hot!", "Hoot!", "Hooot!"} 

 -- wy
 local petWy1 = {"wy1", "wy2", "wy3"} 

 local petWy2 = {"Wyvern (1)", "Wyvern (2)", "Wyvern (3)"} 

 local wyFala = {"Shriek!", "Shriiek!", "Shriiiek!"} 

 -- as
 local petAs1 = {"as1", "as2", "as3"} 

 local petAs2 = {"Ancient Scarab (1)", "Ancient Scarab (2)", "Ancient Scarab (3)"} 

 local asFala = {"Hshi!", "Hsshi!", "Hssshi!"} 

 -- gs
 local petGs1 = {"gs1", "gs2", "gs3"} 

 local petGs2 = {"Giant Spider (1)", "Giant Spider (2)", "Giant Spider (3)"} 

 local gsFala = {"Schu!", "Schuu!", "Schuuu!"} 

 -- dr
 local petDr1 = {"dr1", "dr2", "dr3"} 

 local petDr2 = {"Dragon (1)", "Dragon (2)", "Dragon (3)"} 

 local drFala = {"Groar!", "Grooar!", "Groooar!"} 

 -- df
 local petDf1 = {"df1", "df2", "df3"} 

 local petDf2 = {"Frost Dragon (1)", "Frost Dragon (2)", "Frost Dragon (3)"} 

 local dfFala = {"Zch!", "Zchh!", "Zchhh!"} 

 -- de
 local petDe1 = {"de1", "de2", "de3"} 

 local petDe2 = {"Demon (1)", "Demon (2)", "Demon (3)"} 

 local deFala = {"Muaha!", "Muahaha!", "Muahahaha!"} 

 -- jg
 local petJg1 = {"jg1", "jg2", "jg3"} 

 local petJg2 = {"Juggernaut (1)", "Juggernaut (2)", "Juggernaut (3)"} 

 local jgFala = {"Grar!", "Graar!", "Graaar!"} 


 local petuid = 0

function doPlayerAddSummon(cid, name, pos)
	local MyCreature = doSummonCreature(name, pos)	
	doConvinceCreature(cid, MyCreature)
	return MyCreature
end
 
function onSay(cid, words, param)

 petsuid = 60000
 petonli = 60005
 petslvl = 60010
 petlife = getPlayerStorageValue(cid, 60003)
 petvivo = getPlayerStorageValue(cid, 60011)

 -- rato
 ratolvl = getPlayerStorageValue(cid, 60001)
 ratouid = getPlayerStorageValue(cid, 60002)
 ratoupd = getPlayerStorageValue(cid, 60006)

 -- lobo
 lobolvl = getPlayerStorageValue(cid, 60007)
 lobouid = getPlayerStorageValue(cid, 60008)
 loboupd = getPlayerStorageValue(cid, 60009)

 -- fd
 fdlvl = getPlayerStorageValue(cid, 60012)
 fduid = getPlayerStorageValue(cid, 60013)
 fdupd = getPlayerStorageValue(cid, 60014)

 -- wy
 wylvl = getPlayerStorageValue(cid, 60015)
 wyuid = getPlayerStorageValue(cid, 60021)
 wyupd = getPlayerStorageValue(cid, 60022)

 -- as
 aslvl = getPlayerStorageValue(cid, 60016)
 asuid = getPlayerStorageValue(cid, 60023)
 asupd = getPlayerStorageValue(cid, 60024)

 -- gs
 gslvl = getPlayerStorageValue(cid, 60017)
 gsuid = getPlayerStorageValue(cid, 60025)
 gsupd = getPlayerStorageValue(cid, 60026)

 -- dr
 drlvl = getPlayerStorageValue(cid, 60018)
 druid = getPlayerStorageValue(cid, 60027)
 drupd = getPlayerStorageValue(cid, 60028)

 -- df
 dflvl = getPlayerStorageValue(cid, 60019)
 dfuid = getPlayerStorageValue(cid, 60029)
 dfupd = getPlayerStorageValue(cid, 60030)

 -- de
 delvl = getPlayerStorageValue(cid, 60020)
 deuid = getPlayerStorageValue(cid, 60031)
 deupd = getPlayerStorageValue(cid, 60032)

 -- jg
 jglvl = getPlayerStorageValue(cid, 60035)
 jguid = getPlayerStorageValue(cid, 60033)
 jgupd = getPlayerStorageValue(cid, 60034)

	if getPlayerStorageValue(cid, petsuid) == 0 or getPlayerStorageValue(cid, petsuid) == -1 then	-- nenhum pet
		doPlayerSendTextMessage(cid,24,'Voc� n�o tem nenhum pet.')
	return TRUE
	end

	if getTilePzInfo(getPlayerPosition(cid)) ~= 0 then
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode invocar o pet em protect zone.')
		return TRUE
	end

	-- rato
	if getPlayerStorageValue(cid, petsuid) == 1 and getPlayerStorageValue(cid, petonli) == 1 then	-- rato on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Cave Rat (Level '..ratolvl..').')
		petuid = doPlayerAddSummon(cid, petRato1[ratolvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), ratoFala[ratolvl], 199)
		setPlayerStorageValue(cid, 60002, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, ratolvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Seu Cave Rat (Level '..ratolvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 1 and getPlayerStorageValue(cid, petonli) == 2 then	-- rato off		
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Cave Rat (Level ' .. ratolvl .. ').')
		doSendAnimatedText(getCreaturePosition(ratouid), ratoFala[ratolvl], 199)
		doSendMagicEffect(getCreaturePosition(ratouid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(ratouid) - getCreatureHealth(ratouid))
		doRemoveCreature(ratouid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Seu Cave Rat (Level '..ratolvl..') est� morto.')
	end
	return TRUE
	end

	-- lobo
	if getPlayerStorageValue(cid, petsuid) == 2 and getPlayerStorageValue(cid, petonli) == 1 then	-- lobo on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Wolf (Level '..lobolvl..').')
		petuid = doPlayerAddSummon(cid, petLobo1[lobolvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), loboFala[lobolvl], 199)
		setPlayerStorageValue(cid, 60008, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, lobolvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Seu Wolf (Level '..lobolvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 2 and getPlayerStorageValue(cid, petonli) == 2 then	-- lobo off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Wolf (Level ' .. lobolvl .. ').')
		doSendAnimatedText(getCreaturePosition(lobouid), loboFala[lobolvl], 199)
		doSendMagicEffect(getCreaturePosition(lobouid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(lobouid) - getCreatureHealth(lobouid))
		doRemoveCreature(lobouid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Seu Wolf (Level '..lobolvl..') est� morto.')
	end
	return TRUE
	end

	-- fd
	if getPlayerStorageValue(cid, petsuid) == 3 and getPlayerStorageValue(cid, petonli) == 1 then	-- fd on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Fire Devil (Level '..fdlvl..').')
		petuid = doPlayerAddSummon(cid, petFd1[fdlvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), fdFala[fdlvl], 199)
		setPlayerStorageValue(cid, 60013, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, fdlvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Fire Devil (Level '..fdlvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 3 and getPlayerStorageValue(cid, petonli) == 2 then	-- fd off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Fire Devil (Level ' .. fdlvl .. ').')
		doSendAnimatedText(getCreaturePosition(fduid), fdFala[fdlvl], 199)
		doSendMagicEffect(getCreaturePosition(fduid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(fduid) - getCreatureHealth(fduid))
		doRemoveCreature(fduid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Seu Wolf (Level '..fdlvl..') est� morto.')
	end
	return TRUE
	end

	-- wy
	if getPlayerStorageValue(cid, petsuid) == 4 and getPlayerStorageValue(cid, petonli) == 1 then	-- wy on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Wyvern (Level '..wylvl..').')
		petuid = doPlayerAddSummon(cid, petWy1[wylvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), wyFala[wylvl], 199)
		setPlayerStorageValue(cid, 60021, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, wylvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Seu Wyvern (Level '..wylvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 4 and getPlayerStorageValue(cid, petonli) == 2 then	-- wy off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Wyvern (Level ' .. wylvl .. ').')
		doSendAnimatedText(getCreaturePosition(wyuid), wyFala[wylvl], 199)
		doSendMagicEffect(getCreaturePosition(wyuid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(wyuid) - getCreatureHealth(wyuid))
		doRemoveCreature(wyuid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Wyvern (Level '..wylvl..') est� morto.')
	end
	return TRUE
	end

	-- as
	if getPlayerStorageValue(cid, petsuid) == 5 and getPlayerStorageValue(cid, petonli) == 1 then	-- as on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Ancient Scarab (Level '..aslvl..').')
		petuid = doPlayerAddSummon(cid, petAs1[aslvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), asFala[aslvl], 199)
		setPlayerStorageValue(cid, 60023, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, aslvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Ancient Scarab (Level '..aslvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 5 and getPlayerStorageValue(cid, petonli) == 2 then	-- as off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Ancient Scarab (Level ' .. aslvl .. ').')
		doSendAnimatedText(getCreaturePosition(asuid), asFala[aslvl], 199)
		doSendMagicEffect(getCreaturePosition(asuid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(asuid) - getCreatureHealth(asuid))
		doRemoveCreature(asuid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Ancient Scarab (Level '..aslvl..') est� morto.')
	end
	return TRUE
	end

	-- gs
	if getPlayerStorageValue(cid, petsuid) == 6 and getPlayerStorageValue(cid, petonli) == 1 then	-- gs on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Giant Spider (Level '..gslvl..').')
		petuid = doPlayerAddSummon(cid, petGs1[gslvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), gsFala[gslvl], 199)
		setPlayerStorageValue(cid, 60025, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, gslvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Giant Spider (Level '..gslvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 6 and getPlayerStorageValue(cid, petonli) == 2 then	-- as off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Giant Spider (Level ' .. gslvl .. ').')
		doSendAnimatedText(getCreaturePosition(gsuid), gsFala[gslvl], 199)
		doSendMagicEffect(getCreaturePosition(gsuid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(gsuid) - getCreatureHealth(gsuid))
		doRemoveCreature(gsuid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Giant Spider (Level '..gslvl..') est� morto.')
	end
	return TRUE
	end

	-- dr
	if getPlayerStorageValue(cid, petsuid) == 7 and getPlayerStorageValue(cid, petonli) == 1 then	-- dr on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Dragon (Level '..drlvl..').')
		petuid = doPlayerAddSummon(cid, petDr1[drlvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), drFala[drlvl], 199)
		setPlayerStorageValue(cid, 60027, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, drlvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Dragon (Level '..drlvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 7 and getPlayerStorageValue(cid, petonli) == 2 then	-- dr off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Dragon (Level ' .. drlvl .. ').')
		doSendAnimatedText(getCreaturePosition(druid), drFala[drlvl], 199)
		doSendMagicEffect(getCreaturePosition(druid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(druid) - getCreatureHealth(druid))
		doRemoveCreature(druid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Dragon (Level '..drlvl..') est� morto.')
	end
	return TRUE
	end

	-- df
	if getPlayerStorageValue(cid, petsuid) == 8 and getPlayerStorageValue(cid, petonli) == 1 then	-- df on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Frost Dragon (Level '..dflvl..').')
		petuid = doPlayerAddSummon(cid, petDf1[dflvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), dfFala[dflvl], 199)
		setPlayerStorageValue(cid, 60029, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, dflvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Frost Dragon (Level '..dflvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 8 and getPlayerStorageValue(cid, petonli) == 2 then	-- df off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Frost Dragon (Level ' .. dflvl .. ').')
		doSendAnimatedText(getCreaturePosition(dfuid), dfFala[dflvl], 199)
		doSendMagicEffect(getCreaturePosition(dfuid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(dfuid) - getCreatureHealth(dfuid))
		doRemoveCreature(dfuid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Frost Dragon (Level '..dflvl..') est� morto.')
	end
	return TRUE
	end

	-- de
	if getPlayerStorageValue(cid, petsuid) == 9 and getPlayerStorageValue(cid, petonli) == 1 then	-- de on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Demon (Level '..delvl..').')
		petuid = doPlayerAddSummon(cid, petDe1[delvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), deFala[delvl], 199)
		setPlayerStorageValue(cid, 60031, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, delvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Demon (Level '..delvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 9 and getPlayerStorageValue(cid, petonli) == 2 then	-- de off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Demon (Level ' .. delvl .. ').')
		doSendAnimatedText(getCreaturePosition(deuid), deFala[delvl], 199)
		doSendMagicEffect(getCreaturePosition(deuid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(deuid) - getCreatureHealth(deuid))
		doRemoveCreature(deuid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Demon (Level '..delvl..') est� morto.')
	end
	return TRUE
	end

	-- jg
	if getPlayerStorageValue(cid, petsuid) == 10 and getPlayerStorageValue(cid, petonli) == 1 then	-- jg on
	if getPlayerSummonCount(cid) < 1 then	
	if petvivo == 1 then
		doPlayerSendTextMessage(cid,24,'Pet invocado: Juggernaut (Level '..jglvl..').')
		petuid = doPlayerAddSummon(cid, petJg1[jglvl], getCreaturePosition(cid))
		doSendAnimatedText(getCreaturePosition(petuid), jgFala[jglvl], 199)
		setPlayerStorageValue(cid, 60033, petuid)
		setPlayerStorageValue(cid, petonli, 2)
		setPlayerStorageValue(cid, petslvl, jglvl)
		doCreatureAddHealth(petuid, -getPlayerStorageValue(cid, 60003))
	return TRUE
	else
		doPlayerSendTextMessage(cid,24,'Seu Juggernaut (Level '..jglvl..') est� morto.')
	end
	else
		doPlayerSendTextMessage(cid,24,'Voc� precisa relogar para invocar ou perder o summon.')
	end
	end
	if getPlayerStorageValue(cid, petsuid) == 10 and getPlayerStorageValue(cid, petonli) == 2 then	-- jg off	
	if petvivo == 1 then
	if(hasCondition(cid, CONDITION_INFIGHT) ~= TRUE) then
		doPlayerSendTextMessage(cid,24,'Pet removido: Juggernaut (Level ' .. jglvl .. ').')
		doSendAnimatedText(getCreaturePosition(jguid), jgFala[jglvl], 199)
		doSendMagicEffect(getCreaturePosition(jguid), CONST_ME_POFF)
		setPlayerStorageValue(cid, petonli, 1)
		setPlayerStorageValue(cid, 60003, getCreatureMaxHealth(jguid) - getCreatureHealth(jguid))
		doRemoveCreature(jguid)
	else
		doPlayerSendTextMessage(cid,24,'Voc� n�o pode remove-lo durante uma batalha.')
 		doSendMagicEffect(getPlayerPosition(cid),2)
	end
	else
		doPlayerSendTextMessage(cid,24,'Juggernaut (Level '..jglvl..') est� morto.')
	end
	return TRUE
	end



end