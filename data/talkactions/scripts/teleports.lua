function onSay(cid, words, param)

	tpcarlin = { x=121, y=311, z=7 }
	tpedron = { x=778, y=847, z=8 }
	tpraccoon = { x=160, y=55, z=7 }
	tptirith = { x=448, y=217, z=6 }
	tprecruting = { x=270, y=213, z=7 }
	tplazer = { x=86, y=368, z=8 }
	tpcoli = { x=181, y=399, z=7 }
	tpsiege = { x=543, y=527, z=6 }
	tpbree = { x=891, y=2011, z=7 }

	tp = getPlayerItemCount(cid,13691)
	arena = getTileArenaInfo(getPlayerPosition(cid))



	if words == "/listatp" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,20,"Lista de Teleports encontrados:")
  		doPlayerSendTextMessage(cid,20,"/carlin - Templo de Carlin.")
  		doPlayerSendTextMessage(cid,20,"/edron - Templo de Edron.")
  		doPlayerSendTextMessage(cid,20,"/raccoon - Templo de Raccoon.")
  		doPlayerSendTextMessage(cid,20,"/tirith - Depot de Minas Tirith.")
  		doPlayerSendTextMessage(cid,20,"/recruting - Templo do Centro de Recrutamento.")
  		doPlayerSendTextMessage(cid,20,"/lazer - Area de lazer dos GMs.")
  		doPlayerSendTextMessage(cid,20,"/coli - Area do evento Coliseum.")
  		doPlayerSendTextMessage(cid,20,"/siege - Area do evento Siege.")
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/carlin" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Templo de Carlin (/carlin).")
  		doTeleportThing(cid,tpcarlin)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/edron" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Templo de Edron (/edron).")
  		doTeleportThing(cid,tpedron)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/raccoon" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Templo de Raccoon (/raccoon).")
  		doTeleportThing(cid,tpraccoon)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/tirith" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Depot de Minas Tirith (/tirith).")
  		doTeleportThing(cid,tptirith)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/recruting" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Templo do Centro de Recrutamento (/recruting).")
  		doTeleportThing(cid,tprecruting)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/lazer" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Centro de Lazer dos GMs (/lazer).")
  		doTeleportThing(cid,tplazer)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/coli" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Evento Coliseum (/coli).")
  		doTeleportThing(cid,tpcoli)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/siege" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Evento Siege (/siege).")
  		doTeleportThing(cid,tpsiege)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "/bree" then 
	if getPlayerAccess(cid) >= 3 then
  		doPlayerSendTextMessage(cid,19,"Templo Bree (/bree).")
  		doTeleportThing(cid,tpbree)
	else
  		doPlayerSendCancel(cid,"Somente Gms podem usar este comando.")
	end
	end

	if words == "!carlin" then 
	if getPlayerItemCount(cid,13691) >= 1 then
 	if getTilePzInfo(getPlayerPosition(cid)) == 1 then
	if arena == 0 then
  		doPlayerSendTextMessage(cid,19,"Templo de Carlin (!carlin).")
  		doTeleportThing(cid,tpcarlin)
 		doSendMagicEffect(tpcarlin,10)
 		doSendAnimatedText(tpcarlin,"Wuon!",214)
		doPlayerTakeItem(cid,13691,1)
		doPlayerSendTextMessage(cid, 20, 'Ainda lhe resta ' .. tp - 1 .. ' Teleports.')
	else
 		doPlayerSendTextMessage(cid, 19, "Voc� n�o pode usar teleport neste local.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� s� pode se teleportar em protect zone.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� n�o tem nenhum teleport.")
	end
	end

	if words == "!edron" then 
	if getPlayerItemCount(cid,13691) >= 1 then
 	if getTilePzInfo(getPlayerPosition(cid)) == 1 then
	if arena == 0 then
  		doPlayerSendTextMessage(cid,19,"Templo de Edron (!edron).")
  		doTeleportThing(cid,tpedron)
 		doSendMagicEffect(tpedron,10)
 		doSendAnimatedText(tpedron,"Wuon!",214)
		doPlayerTakeItem(cid,13691,1)
		doPlayerSendTextMessage(cid, 20, 'Ainda lhe resta ' .. tp - 1 .. ' Teleports.')
	else
 		doPlayerSendTextMessage(cid, 19, "Voc� n�o pode usar teleport neste local.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� s� pode se teleportar em protect zone.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� n�o tem nenhum teleport.")
	end
	end

	if words == "!raccoon" then 
	if getPlayerItemCount(cid,13691) >= 1 then
 	if getTilePzInfo(getPlayerPosition(cid)) == 1 then
	if arena == 0 then
  		doPlayerSendTextMessage(cid,19,"Templo de Raccoon (!raccoon).")
  		doTeleportThing(cid,tpraccoon)
 		doSendMagicEffect(tpraccoon,10)
 		doSendAnimatedText(tpraccoon,"Wuon!",214)
		doPlayerTakeItem(cid,13691,1)
		doPlayerSendTextMessage(cid, 20, 'Ainda lhe resta ' .. tp - 1 .. ' Teleports.')
	else
 		doPlayerSendTextMessage(cid, 19, "Voc� n�o pode usar teleport neste local.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� s� pode se teleportar em protect zone.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� n�o tem nenhum teleport.")
	end
	end

	if words == "!tirith" then 
	if getPlayerItemCount(cid,13691) >= 1 then
 	if getTilePzInfo(getPlayerPosition(cid)) == 1 then
	if arena == 0 then
  		doPlayerSendTextMessage(cid,19,"Depot de Minas Tirith (!tirith).")
  		doTeleportThing(cid,tptirith)
 		doSendMagicEffect(tptirith,10)
 		doSendAnimatedText(tptirith,"Wuon!",214)
		doPlayerTakeItem(cid,13691,1)
		doPlayerSendTextMessage(cid, 20, 'Ainda lhe resta ' .. tp - 1 .. ' Teleports.')
	else
 		doPlayerSendTextMessage(cid, 19, "Voc� n�o pode usar teleport neste local.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� s� pode se teleportar em protect zone.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� n�o tem nenhum teleport.")
	end
	end

	if words == "!bree" then 
	if getPlayerItemCount(cid,13691) >= 1 then
 	if getTilePzInfo(getPlayerPosition(cid)) == 1 then
	if arena == 0 then
  		doPlayerSendTextMessage(cid,19,"Templo de Bree (!bree).")
  		doTeleportThing(cid,tpbree)
 		doSendMagicEffect(tpbree,10)
 		doSendAnimatedText(tpbree,"Wuon!",214)
		doPlayerTakeItem(cid,13691,1)
		doPlayerSendTextMessage(cid, 20, 'Ainda lhe resta ' .. tp - 1 .. ' Teleports.')
	else
 		doPlayerSendTextMessage(cid, 19, "Voc� n�o pode usar teleport neste local.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� s� pode se teleportar em protect zone.")
	end
	else
  		doPlayerSendCancel(cid,"Voc� n�o tem nenhum teleport.")
	end
	end


end