local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,2)
min = -((skill*0)+level)
max = -((skill*4)+level)
return min, max
end

setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,2)
min = -((skill*12)+level)
max = -((skill*14)+level)
return min, max
end

setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat3 = createCombatObject()
setCombatParam(combat3, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,2)
min = -((skill*12)+level)
max = -((skill*16)+level)
return min, max
end

setCombatCallback(combat3, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -60)
setCombatCondition(combat2, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -60)
setCombatCondition(combat3, condition)

function onUseWeapon(cid, var)
Critical = math.random(1,100)
if Critical > 80 then
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat2, var)
end

Critical = math.random(1,100)
if Critical < 2 then
doPlayerSay(cid,"For Honor!",16)
doPlayerAddHealth(cid, math.random(1000, 10000))
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat3, var)
else
doCombat(cid, combat1, var)
end
end