local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
level = getPlayerLevel(cid)
min = -(skill*25)
max = -((skill*25)+level*5)
return min, max
end

setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
level = getPlayerLevel(cid)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat3 = createCombatObject()
setCombatParam(combat3, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
level = getPlayerLevel(cid)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat3, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat4 = createCombatObject()
setCombatParam(combat4, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat4, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat4, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat5 = createCombatObject()
setCombatParam(combat5, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat5, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat5, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat6 = createCombatObject()
setCombatParam(combat6, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat6, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat6, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat7 = createCombatObject()
setCombatParam(combat7, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat7, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat7, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat8 = createCombatObject()
setCombatParam(combat8, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat8, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat8, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat9 = createCombatObject()
setCombatParam(combat9, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat9, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
min = -((skill*25)+level*9)
max = -((skill*26)+level*9)
return min, max
end

setCombatCallback(combat9, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -120)
setCombatCondition(combat2, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -120)
setCombatCondition(combat3, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -120)
setCombatCondition(combat4, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -142)
setCombatCondition(combat5, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -142)
setCombatCondition(combat6, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -162)
setCombatCondition(combat7, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -176)
setCombatCondition(combat8, condition)

local condition = createConditionObject(CONDITION_EMO)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3000, -184)
setCombatCondition(combat9, condition)

function onUseWeapon(cid, var)
Critical = math.random(1,100)
if getPlayerSkill(cid,3) <= 39 and Critical > 94 then
life = 5*getPlayerMaxHealth(cid)/100
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat2, var)
end

Critical = math.random(1,100)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 40 and Critical > 96 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat3, var)
end

Critical = math.random(1,110)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 59 and Critical > 95 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat4, var)
end

Critical = math.random(1,110)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 84 and Critical > 105 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat5, var)
end

Critical = math.random(1,110)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 101 and Critical > 107 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat6, var)
end

Critical = math.random(1,100)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 1 and Critical > 97 then
doPlayerAddHealth(cid, life)
doPlayerSay(cid,"For Honor!",16)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat7, var)
end

Critical = math.random(1,100)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 119 and Critical > 98 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat8, var)
end

Critical = math.random(1,100)
life = 10*getPlayerMaxHealth(cid)/100
if getPlayerSkill(cid,3) >= 140 and Critical > 96 then
doPlayerAddHealth(cid, life)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat9, var)
else
doCombat(cid, combat1, var)
end
end