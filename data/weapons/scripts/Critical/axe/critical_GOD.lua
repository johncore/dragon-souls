local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
level = getPlayerLevel(cid)
min = -((skill*22)+level*7)
max = -((skill*28)+level*8)
return min, max
end

setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,3)
level = getPlayerLevel(cid)
min = -((skill*54)+level*11)
max = -((skill*60)+level*11)
return min, max
end

local condition = createConditionObject(CONDITION_EMO)
rande = math.random(279,341)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 100, 3100, -rande)
setCombatCondition(combat2, condition)

setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

function onUseWeapon(cid, var)
	fala = math.random(1,10)
	rand = math.random(1,500)
	if rand <= getPlayerSkill(cid,3) then
	if fala == 1 then
 	doPlayerSay(cid,"I'am God of war!",16)
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
    	doCombat(cid, combat2, var)
else
 	--doPlayerSay(cid,"My God weapons!",16)
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
    	doCombat(cid, combat2, var)
end
else
    	doCombat(cid, combat1, var)
end
end