local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_BULLET)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,4)
level = getPlayerLevel(cid)
min = -((skill*25)+level*5+maglevel*2)
max = -((skill*26)+level*6+maglevel*2)
return min, max
end

setCombatCallback(combat1, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 31)
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_BULLET)
function onGetFormulaValues(cid, level, maglevel)
skill = getPlayerSkill(cid,4)
level = getPlayerLevel(cid)
min = -((skill*32)+level*7+maglevel*4)
max = -((skill*34)+level*8+maglevel*4)
return min, max
end

setCombatCallback(combat2, CALLBACK_PARAM_LEVELMAGICVALUE, "onGetFormulaValues")

local condition = createConditionObject(CONDITION_PARALYZE)
setConditionParam(condition, CONDITION_PARAM_TICKS, 1000)
setConditionFormula(condition, -0.8, 0, -0.8, 0)
setCombatCondition(combat2, condition)

setCombatCondition(combat2, condition)

local hemorragia = createConditionObject(CONDITION_EMO)
setConditionParam(hemorragia, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(hemorragia, 1, 2000, -4000)
addDamageCondition(hemorragia, 1, 2000, -2000)
addDamageCondition(hemorragia, 1, 2000, -1000)

function onUseWeapon(cid, var)

    local function darkness(target)
     if isPlayer(target) == 1 then
     doSendAnimatedText(getThingPos(target),"Weakness!",255)
             doPlayerSay(target,"That hurts!",16)
        else
         doSendAnimatedText(getThingPos(target),"Weakness!",255)
                     end
            end

     local target = getCreatureTarget(cid)
     local black = 
   { lookType = getCreatureOutfit(target).lookType,
     lookHead = getCreatureOutfit(target).lookHead,
     lookBody = getCreatureOutfit(target).lookBody,
     lookLegs = getCreatureOutfit(target).lookLegs,
     lookFeet = getCreatureOutfit(target).lookFeet, 
     lookAddons = getCreatureOutfit(target).lookAddons }
	fala = math.random(10,10)
	rand = math.random(1,1000)
     if(target ~= 0) then
	if rand <= getPlayerSkill(cid,4) then
	if fala == 10 then
 	doPlayerSay(cid,"Fell the darkness!",16)
     doSetCreatureOutfit(target, black, 2000)
     addEvent(darkness, 1*500,target)
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
     doPlayerAddMana(cid,(getPlayerMaxMana(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
     doTargetCombatCondition(0, target, hemorragia, CONST_ME_NONE)
    	doCombat(cid, combat2, var)
else
	doPlayerAddHealth(cid,(getPlayerMaxHealth(cid)/10))
 	doSendAnimatedText(getPlayerPosition(cid),"Critical!",129)
    	doCombat(cid, combat2, var)
end
else
    	doCombat(cid, combat1, var)
end
end
end
