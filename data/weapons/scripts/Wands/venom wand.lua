local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_POISON)
setCombatFormula(combat, COMBAT_FORMULA_LEVELMAGIC, -2, -1000, -2, -1400)

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_POISON)
setCombatFormula(combat2, COMBAT_FORMULA_LEVELMAGIC, -2, -1400, -2, -1600)

local combat3 = createCombatObject()
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat3, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat3, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_POISON)
setCombatFormula(combat3, COMBAT_FORMULA_LEVELMAGIC, -2, -1200, -2, -1400)

local condition = createConditionObject(CONDITION_POISON)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 1, 2000, -500)
addDamageCondition(condition, 1, 2000, -250)
addDamageCondition(condition, 1, 2000, -100)
setCombatCondition(combat2, condition)

function onUseWeapon(cid, var)
rand = math.random(90,100)
if rand == 90 then
doPlayerSay(cid, "The Sickness!!",16)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_PLATINUMBLUE)
doCombat(cid, combat2, var)
end
rand = math.random(95,100)
if rand == 95 then
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_PLATINUMBLUE)
rand = math.random(95,100)
doCombat(cid, combat3, var)
else
doCombat(cid, combat, var)
end
end