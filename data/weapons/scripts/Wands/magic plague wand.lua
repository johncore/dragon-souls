local combat = createCombatObject()
setCombatParam(combat, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_POISON)
setCombatFormula(combat, COMBAT_FORMULA_LEVELMAGIC, -3, -3300, -3, -3750)

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, 37)
setCombatFormula(combat2, COMBAT_FORMULA_LEVELMAGIC, -3, -4200, -3, -4550)

local combat3 = createCombatObject()
setCombatParam(combat3, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat3, COMBAT_PARAM_EFFECT, CONST_ME_POISONAREA)
setCombatParam(combat3, COMBAT_PARAM_DISTANCEEFFECT, 37)
setCombatFormula(combat3, COMBAT_FORMULA_LEVELMAGIC, -3, -4500, -3, -5750)

local condition = createConditionObject(CONDITION_POISON)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 1, 2000, -4000)
addDamageCondition(condition, 1, 2000, -2000)
addDamageCondition(condition, 1, 2000, -1000)
setCombatCondition(combat2, condition)

local condition = createConditionObject(CONDITION_POISON)
setConditionParam(condition, CONDITION_PARAM_DELAYED, 1)
addDamageCondition(condition, 1, 2000, -4000)
addDamageCondition(condition, 1, 2000, -2000)
addDamageCondition(condition, 1, 2000, -1000)
setCombatCondition(combat3, condition)

function onUseWeapon(cid, var)
rand = math.random(1,20)
if rand == 5 then
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
doCombat(cid, combat2, var)
end
rand = math.random(1,20)
if rand == 1 then
doPlayerSay(cid, "The Sickness!!",16)
doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
rand = math.random(95,100)
doCombat(cid, combat3, var)
else
doCombat(cid, combat, var)
end
end