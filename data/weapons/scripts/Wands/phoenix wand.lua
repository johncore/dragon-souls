local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
setCombatParam(combat1, COMBAT_PARAM_EFFECT, 15)
setCombatFormula(combat1, COMBAT_FORMULA_LEVELMAGIC, -3, -2000, -3, -2508)

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 15)
setCombatFormula(combat2, COMBAT_FORMULA_LEVELMAGIC, -3, -3200, -3, -3798)

function onUseWeapon(cid, var)
dice = math.random(1, 200)
critrate = getPlayerSkill(cid, 0)

	if critrate >= dice then
     doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
		doPlayerSay(cid, "Burn in the Hell!", 16)
		return doCombat(cid, combat2, var)
	else 
		return doCombat(cid, combat1, var)
	end
end