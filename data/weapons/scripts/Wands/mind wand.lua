local combat1 = createCombatObject()
setCombatParam(combat1, COMBAT_PARAM_BLOCKARMOR, 200)
setCombatParam(combat1, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat1, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
setCombatParam(combat1, COMBAT_PARAM_EFFECT, 5)
setCombatFormula(combat1, COMBAT_FORMULA_LEVELMAGIC, -2, -900, -2, -1000)

local combat2 = createCombatObject()
setCombatParam(combat2, COMBAT_PARAM_TYPE, COMBAT_PHYSICALDAMAGE)
setCombatParam(combat2, COMBAT_PARAM_DISTANCEEFFECT, CONST_ANI_FIRE)
setCombatParam(combat2, COMBAT_PARAM_EFFECT, 15)
setCombatFormula(combat2, COMBAT_FORMULA_LEVELMAGIC, -2, -800, -2, -900)

function onUseWeapon(cid, var)
dice = math.random(1, 200)
critrate = getPlayerSkill(cid, 0)

	if critrate >= dice then
     doSendAnimatedText(getPlayerPosition(cid), "Critical!", TEXTCOLOR_LIGHTGREY)
		doPlayerSay(cid, "Burn !", 16)
		return doCombat(cid, combat2, var)
	else 
		return doCombat(cid, combat1, var)
	end
end