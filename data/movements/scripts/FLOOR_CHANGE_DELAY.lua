local Delay = createConditionObject(CONDITION_FLOOR_CHANGE)
setConditionParam(Delay, CONDITION_PARAM_TICKS, 3000)

function onStepIn(cid, item, position, fromPosition)

doTargetCombatCondition(0, cid, Delay, CONST_ME_NONE)
end